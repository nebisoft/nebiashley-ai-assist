.PHONY: build

build:
	rm -rf ./build/exe.nebios/
	mkdir -p ./build/exe.nebios/unpacked
	cp ./NebiAshley.py ./build/exe.nebios/unpacked
	cp -r ./lib ./build/exe.nebios/unpacked
	cp -r ./ui ./build/exe.nebios/unpacked
	python3 -m compileall ./build/exe.nebios/unpacked
	python3 -m venv ./build/exe.nebios/unpacked/venv
	./build/exe.nebios/unpacked/venv/bin/pip install requests openai numpy wave pyaudio 
	echo '#!/bin/bash' > ./build/exe.nebios/unpacked/app.exec
	echo 'export PATH=$$PWD/venv/bin:$$PATH' >> ./build/exe.nebios/unpacked/app.exec
	echo 'bash $$PWD/venv/bin/activate' >> ./build/exe.nebios/unpacked/app.exec
	echo 'python3 ./NebiAshley.py $$@' >> ./build/exe.nebios/unpacked/app.exec
	chmod +x ./build/exe.nebios/unpacked/app.exec
	cp ./icon.png ./build/exe.nebios/NebiAshley.png
	cp ./NebiAshley.ninf ./build/exe.nebios/NebiAshley.ninf
	exec2napp ./build/exe.nebios/unpacked/ ./build/exe.nebios/NebiAshley.ninf ./build/exe.nebios/NebiAshley.napp

