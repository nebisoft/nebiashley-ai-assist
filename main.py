import gi
import threading
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib
from lib.ashley import API

class AshleyApp:
	def __init__(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("./ui/app.glade")
		
		self.api = API()
		
		style_provider = Gtk.CssProvider()
		style_provider.load_from_path('./ui/app.css')
		Gtk.StyleContext.add_provider_for_screen(
			Gdk.Screen.get_default(),
			style_provider,
			Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
		)

		self.window = self.builder.get_object("ashley_window")
		
		self.scrolled = self.builder.get_object("ashley_scrolled")
		self.scrolled_viewport = self.builder.get_object("ashley_scrolled")
		
		self.msgbox = self.builder.get_object("ashley_msgbox")
		
		self.record_button = self.builder.get_object("ashley_mic_btn")
		self.record_button_image = self.builder.get_object("ashley_mic_img")
		self.text_input = self.builder.get_object("ashley_text_input")
		self.send_button = self.builder.get_object("ashley_sent_btn")
		
		self.send_button.connect("clicked", self.on_send_button_clicked)
		
		self.window.connect("destroy", exit)
		self.window.show_all()
		
		self.record_button.hide()
		
	def create_gpt_chat_label(self):
		self.builder.add_from_file("./ui/chat_labels.glade")
		gpt_label = self.builder.get_object("ashley_gpt_label")
		gpt_label.set_label("Waiting...")
		self.msgbox.add(gpt_label)
		self.msgbox.show_all()
		return gpt_label
			
	def create_user_chat_label(self, text):
		self.builder.add_from_file("./ui/chat_labels.glade")
		user_label = self.builder.get_object("ashley_user_label")
		user_label.set_label(text)
		self.msgbox.add(user_label)
		self.msgbox.show_all()
		
	def set_gpt_text(self, txt):
		final_txt = txt.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("**NebiAshley:** ", "")
		GLib.idle_add(self.current_gpt_label.set_markup, final_txt)

	def on_send_button_clicked(self, btn):
		input_txt = self.text_input.get_text()
		if input_txt:
			self.create_user_chat_label(input_txt)
			self.text_input.set_text("")
			self.current_gpt_label = self.create_gpt_chat_label()
			thread = threading.Thread(target=self.api.send_message, args=(input_txt, self))
			thread.start()

if __name__ == "__main__":
	app = AshleyApp()
	Gtk.main()

