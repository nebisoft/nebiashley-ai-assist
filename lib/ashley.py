import openai
import requests
import subprocess
import pyaudio
import wave
import numpy as np
import os

# API key does not need to be valid
openai.base_url = "https://aiapi.nebisoftware.com/"
openai.api_key = 'sk-XXXXXXXXXXXXXXXXXXXX'

class API:	
	def talk_text(self, text):
		"""
		This function converts text to speech and plays the audio.
		"""
		tts_url = f'https://aiapi.nebisoftware.com/v1/audio/speech'
		tts_headers = {
			'Content-Type': 'application/json'
		}
		tts_data = {
			"model": "tts-1",
			"input": text,
			"voice": "alloy"
		}

		# Send POST request for TTS
		tts_response = requests.post(tts_url, headers=tts_headers, json=tts_data)

		# Check TTS response
		if tts_response.status_code == 200:
			# Save MP3 file as /tmp/nebiashley_tts.mp3
			mp3_file_path = '/tmp/nebiashley_tts.mp3'
			with open(mp3_file_path, 'wb') as f:
				f.write(tts_response.content)
			
			# Play MP3 file using gst
			play_command = f'bash -c "gst-play-1.0 {mp3_file_path} &> /dev/null" &'
			subprocess.run(play_command, shell=True)

	def get_system_prompt(self):
		prompt_url = "https://intelligence.nebisoftware.com/ai_prompt.txt"
		try:
			response = requests.get(prompt_url)
			if response.status_code == 200:
				return response.text.strip()
			else:
				print(f"Failed to fetch system prompt. Status code: {response.status_code}")
				return ""
		except requests.exceptions.RequestException as e:
			print(f"Error fetching system prompt: {e}")
			return ""

	def send_message(self, gpt_input, client=None):
		system_prompt = self.get_system_prompt()
		
		completion = openai.chat.completions.create(
			model="gpt-4",
			stream=True,
			messages=[
				{
					"role": "user",
					"content": gpt_input
				},
				{
					"role": "system",
					"content": system_prompt
				}
			]
		)
		completion_full = ""
		is_talked = False
		need_talk = False

		for chunk in completion:
			chunk_content = chunk.choices[0].delta.content.replace('**NebiAshley:** ', '')
			completion_full += chunk_content
			if client:
				client.set_gpt_text(completion_full)
			if '\n' in completion_full and not is_talked:
				need_talk = True
			else:
				need_talk = False
			if need_talk:
				is_talked = True
				talk_text = completion_full.split("\n", 1)[0].replace("**NebiAshley:** ", "")
				self.talk_text(talk_text)
				
		print("")
		if not is_talked:
			talk_text = completion_full.split("\n", 1)[0].replace("**NebiAshley:** ", "")
			self.talk_text(talk_text)

	def transcribe_audio(self, file_path):
		stt_url = "https://aiapi.nebisoftware.com/v1/audio/transcriptions"
		files = {
			'file': open(file_path, 'rb')
		}
		data = {
			'model': 'whisper-1',
			'language': 'en'
		}

		# Send POST request for STT
		stt_response = requests.post(stt_url, files=files, data=data)

		# Check STT response
		if stt_response.status_code == 200:
			return stt_response.json().get('text', '').replace("[BLANK_AUDIO]", "").replace("[Music]", "")
		else:
			print(f"Failed to transcribe audio. Status code: {stt_response.status_code}")
			return ""

	@staticmethod
	def record_audio(self, filename="/tmp/nebiashley_stt.wav", threshold=500.0, silence_duration=2, max_duration=30):
		FORMAT = pyaudio.paInt16
		CHANNELS = 1
		RATE = 44100
		CHUNK = 1024
		RECORD_SECONDS = max_duration
		WAVE_OUTPUT_FILENAME = filename

		audio = pyaudio.PyAudio()

		# start Recording
		stream = audio.open(format=FORMAT, channels=CHANNELS,
						rate=RATE, input=True,
						frames_per_buffer=CHUNK)
		print("Recording...")

		frames = []
		silent_chunks = 0

		for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
			data = stream.read(CHUNK)
			frames.append(data)
			volume = np.frombuffer(data, dtype=np.int16).astype(np.float32).mean()
			if volume < threshold:
				silent_chunks += 1
			else:
				silent_chunks = 0
			if silent_chunks > (silence_duration * RATE / CHUNK):
				break

		# stop Recording
		stream.stop_stream()
		stream.close()
		audio.terminate()
		
		print("Recording finished")

		waveFile = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
		waveFile.setnchannels(CHANNELS)
		waveFile.setsampwidth(audio.get_sample_size(FORMAT))
		waveFile.setframerate(RATE)
		waveFile.writeframes(b''.join(frames))
		waveFile.close()
		return filename
